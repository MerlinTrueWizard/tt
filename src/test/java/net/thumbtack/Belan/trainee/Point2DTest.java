package net.thumbtack.Belan.trainee;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Станислав on 11.09.2016.
 */
public class Point2DTest {
    //Point2D p = new Point2D();

    @Test
    public void defaultConstructorTest() {
        Point2D p = new Point2D();

        assertEquals(0,p.getX(),0);
        assertEquals(0,p.getY(),0);
    }

    @Test
    public void ConstructorTest(){
        Point2D p = new Point2D(4, 3);

        assertEquals(4, p.getX(),0);
        assertEquals(3, p.getY(),0);
    }

    @Test
    public void MoveTest(){
        Point2D p = new Point2D(2,4);
        p.movePoint(0.2, 0.4);

        assertEquals(2.2, p.getX(), 0);
        assertEquals(4.4, p.getY(), 0);
    }


}
