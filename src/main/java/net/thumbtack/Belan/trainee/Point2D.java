package net.thumbtack.Belan.trainee;

/**
 * Created by Станислав on 11.09.2016.
 */
public class Point2D {
    private double x;
    private double y;

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Point2D(){
        setX(0);
        setY(0);
    }

    public Point2D(double x, double y){
        setX(x);
        setY(y);
    }

    public void Print(){
        System.out.println("(" + getX()+";"+ getY() + ")");
    }

    public void movePoint(double dx, double dy){
        setY(y+dy);
        setX(x+dx);
    }
}
